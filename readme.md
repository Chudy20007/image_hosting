<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Introduction

Image hosting is an basic application based on the framework Laravel (with JavaScript and jQuery). Application allows you to manage your images and albums from the holidays!

## Features

*  CRUD for the all controllers with basic validation
*  Security against: CORS, CSRF , XSS, SQL Injection, Bots
*  Authorization type:  Basic Laravel Auth
*  User interface: recovering and resetting password

## Used Components

* Middleware
* Requests
* Enities
* Migrations
* Seeders

## Used packages

* [LaravelCollective/html - MIT](https://github.com/LaravelCollective/html)


## Installation

1.  Clone the project: `git clone https://github.com/Chudy20007/image-hosting`.
2.  In the CLI (composer) pass this command: `composer install `. 
3.  Create a database and configure **.env** .
4.  Run the migrations with seeders (important! don't change the values 'names' in the 'roles' table): `php artisan migrate:fresh` , `php artisan db:seed`.
5.  Run application: `php artisan:serve`.

## Configuration

*You must specify the basic environment variables in the **.env** file.*

## Available URL

<table style='width:100%'>
    <tr>
        <td width='10%'>
            <h4>HTTP Method</h4>
        </td>
        <td width='10%'>
            <h4>Route</h4>
        </td>
        <td width='80%'>
            <h4>Corresponding Action</h4>
        </td>
    </tr>
    <tr>
        <td>GET</td>
        <td>user_panel</td>
        <td>App\Http\Controllers\UserController@user_panel</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums_panel</td>
        <td>App\Http\Controllers\UserController@albums_panel</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>contact</td>
        <td>App\Http\Controllers\StaticPagesController@contact</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>about</td>
        <td>App\Http\Controllers\StaticPagesController@about</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>home</td>
        <td>App\Http\Controllers\StaticPagesController@home</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>get_job</td>
        <td>App\Http\Controllers\StaticPagesController@get_job</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures</td>
        <td>App\Http\Controllers\PicturesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>user/{id}//update</td>
        <td>App\Http\Controllers\UserController@update</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums</td>
        <td>App\Http\Controllers\AlbumsController@albums_list</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>user/{id}/edit</td>
        <td>App\Http\Controllers\AdminController@edit</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>index</td>
        <td>App\Http\Controllers\CommentsController@store</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>album_comment/store_com</td>
        <td>App\Http\Controllers\CommentsController@store_com</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums_list</td>
        <td>App\Http\Controllers\AlbumsController@albums_list</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums</td>
        <td>App\Http\Controllers\AlbumsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>edit</td>
        <td>App\Http\Controllers\UserController@edit</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>create_comment</td>
        <td>App\Http\Controllers\PicturesController@create_comment</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/create</td>
        <td>App\Http\Controllers\AlbumsController@create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures/{id}/destroy</td>
        <td>App\Http\Controllers\PicturesController@destroy</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/add_viewers</td>
        <td>App\Http\Controllers\AlbumsController@store_viewers</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/{id}/add_visitors</td>
        <td>App\Http\Controllers\AlbumsController@show_visitors_form</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/add_viewers</td>
        <td>App\Http\Controllers\PicturesController@store_viewers</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures/{id}/add_visitors</td>
        <td>App\Http\Controllers\PicturesController@show_visitors_form</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>albums/{id}</td>
        <td>App\Http\Controllers\AlbumsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/{id}</td>
        <td>App\Http\Controllers\AlbumsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/{id}/edit</td>
        <td>App\Http\Controllers\AlbumsController@edit</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>albums/{id}/update</td>
        <td>App\Http\Controllers\AlbumsController@edit_pic</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/user/{id}</td>
        <td>App\Http\Controllers\AlbumsController@select_albums_user</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>logout</td>
        <td>\App\Http\Controllers\Auth\LoginController@logout</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comment/{id}/create</td>
        <td>App\Http\Controllers\CommentsController@create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>user/{id}</td>
        <td>App\Http\Controllers\UserController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>album_comment/{id}/create</td>
        <td>App\Http\Controllers\CommentsController@album_comment_create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>album_comment/{id}/edit</td>
        <td>App\Http\Controllers\CommentsController@album_com_edit</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums/{id}/add_pictures</td>
        <td>App\Http\Controllers\AlbumsController@add_pictures</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/store_pic</td>
        <td>App\Http\Controllers\AlbumsController@store_pic</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>user/{id}/edit</td>
        <td>App\Http\Controllers\AdminController@edit</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums_ratings_list/{id}/edit</td>
        <td>App\Http\Controllers\ImageRatingController@edit_album_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/{id}/edit</td>
        <td>App\Http\Controllers\AlbumsController@edit</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums_list_a</td>
        <td>App\Http\Controllers\AdminController@albums_list</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>album_comments/{id}/edit</td>
        <td>App\Http\Controllers\CommentsController@album_com_update</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>pictures_ratings_list/{id}/activate</td>
        <td>App\Http\Controllers\AdminController@activate_rate</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>pictures_ratings_list/{id}/destroy</td>
        <td>App\Http\Controllers\AdminController@destroy_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures_ratings_list/{id}/edit</td>
        <td>App\Http\Controllers\ImageRatingController@edit_rate</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>albums_ratings_list/{id}/activate</td>
        <td>App\Http\Controllers\AdminController@activate_album_rate</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>albums_ratings_list/{id}/destroy</td>
        <td>App\Http\Controllers\AdminController@destroy_album_rate</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures_list</td>
        <td>App\Http\Controllers\AdminController@pictures_list</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/{id}/edit</td>
        <td>App\Http\Controllers\PicturesController@edit</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>admin_panel</td>
        <td>App\Http\Controllers\AdminController@admin_panel</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>comments_list/{id}/activate</td>
        <td>App\Http\Controllers\CommentsController@activate</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>pictures/{id}/destroy</td>
        <td>App\Http\Controllers\PicturesController@destroy</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>users_list/{id}/activate</td>
        <td>App\Http\Controllers\AdminController@activate</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>pictures_list/{id}/activate</td>
        <td>App\Http\Controllers\PicturesController@activate</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>users_list/{id}/destroy</td>
        <td>App\Http\Controllers\AdminController@destroy</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>album_comment/{id}/edit</td>
        <td>App\Http\Controllers\CommentsController@album_com_edit</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>album_comments/{id}/destroy</td>
        <td>App\Http\Controllers\AdminController@album_comment_destroy</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>album_comments/{id}/activate</td>
        <td>App\Http\Controllers\AdminController@album_comment_activate</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>users_list</td>
        <td>App\Http\Controllers\AdminController@usersList</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>album_comments_list</td>
        <td>App\Http\Controllers\AdminController@album_comments_list</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comments_list</td>
        <td>App\Http\Controllers\AdminController@comments_list</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures_ratings_list</td>
        <td>App\Http\Controllers\AdminController@pictures_ratings_list</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>albums_ratings_list</td>
        <td>App\Http\Controllers\AdminController@albums_ratings_list</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>albums/{id}/destroy</td>
        <td>App\Http\Controllers\AlbumsController@destroy</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>albums/{id}/activate</td>
        <td>App\Http\Controllers\AlbumsController@activate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>user/{id}/update</td>
        <td>App\Http\Controllers\AdminController@update</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>user/store</td>
        <td>App\Http\Controllers\AdminController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>user/create</td>
        <td>App\Http\Controllers\AdminController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>user_panel/find_pictures</td>
        <td>App\Http\Controllers\PicturesController@find_pictures</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums_panel/find_albums</td>
        <td>App\Http\Controllers\AlbumsController@find_albums</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/{id}/store_rating</td>
        <td>App\Http\Controllers\ImageRatingController@store_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/{id}/store_rating</td>
        <td>App\Http\Controllers\ImageRatingController@store_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/store_rating</td>
        <td>App\Http\Controllers\ImageRatingController@store_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/store_rating</td>
        <td>App\Http\Controllers\AlbumRatingController@store_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>user/{id}/store_rating</td>
        <td>App\Http\Controllers\ImageRatingController@store_rate</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/user/{id}/store_rating</td>
        <td>App\Http\Controllers\AlbumRatingController@store_rate</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures</td>
        <td>App\Http\Controllers\PicturesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>show_pic/{uploadLink}</td>
        <td>App\Http\Controllers\PicturesController@show_pic</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>show_alb/{uploadLink}</td>
        <td>App\Http\Controllers\AlbumsController@show_alb</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/find_pictures</td>
        <td>App\Http\Controllers\PicturesController@find_pictures</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>albums/find_albums</td>
        <td>App\Http\Controllers\AlbumsController@find_albums</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>login</td>
        <td>App\Http\Controllers\Auth\LoginController@showLoginForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>login</td>
        <td>App\Http\Controllers\Auth\LoginController@login</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>logout</td>
        <td>App\Http\Controllers\Auth\LoginController@logout</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>register</td>
        <td>App\Http\Controllers\Auth\RegisterController@showRegistrationForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>register</td>
        <td>App\Http\Controllers\Auth\RegisterController@register</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>password/reset</td>
        <td>App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>password/email</td>
        <td>App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>password/reset/{token}</td>
        <td>App\Http\Controllers\Auth\ResetPasswordController@showResetForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>password/reset</td>
        <td>App\Http\Controllers\Auth\ResetPasswordController@reset</td>
    </tr>
    <tr>
        <td>PATCH</td>
        <td>pictures/{id}update</td>
        <td>App\Http\Controllers\PicturesController@update</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures/{id}/edit</td>
        <td>App\Http\Controllers\PicturesController@edit</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>comment/{id}/edit</td>
        <td>App\Http\Controllers\CommentsController@edit</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures/create</td>
        <td>App\Http\Controllers\PicturesController@create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>pictures/{id}</td>
        <td>App\Http\Controllers\PicturesController@show</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>pictures/store</td>
        <td>App\Http\Controllers\PicturesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comment</td>
        <td>App\Http\Controllers\CommentsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comment/create</td>
        <td>App\Http\Controllers\CommentsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>comment</td>
        <td>App\Http\Controllers\CommentsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comment/{comment}</td>
        <td>App\Http\Controllers\CommentsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>comment/{comment}/edit</td>
        <td>App\Http\Controllers\CommentsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>comment/{comment}</td>
        <td>App\Http\Controllers\CommentsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>comment/{comment}</td>
        <td>App\Http\Controllers\CommentsController@destroy</td>
    </tr>
</table>


## License
"Image hosting is an open-sourced software licensed under the MIT license(https://opensource.org/licenses/MIT).
